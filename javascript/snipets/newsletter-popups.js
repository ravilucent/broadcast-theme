import { setCookie, getCookie} from '../helper'
var newsletterpopup=document.querySelector('.newsletter-popup');
const time=newsletterpopup.getAttribute('data-time');
class Newsletterpopup{

    constructor(){
        this.cookieName="subscriber";
        this.modalDiv=".newsletter-popup";
        this.formNewsletter=".newsletterform";
        this.newsletterEmail=".email-data";
        this.newsletterErrorMsg=".newsletter-error-sub";
        this.finalSubmit=".shopify-challenge__button";
        this.robotForm=".shopify-challenge__container form";
        this.checkCookie();
        this.clickEvent();
    }

    checkCookie=()=>{
        const that=this;
        if(!getCookie(that.cookieName)){
            document.querySelector(that.modalDiv).style.display='block';
        }
    }

    setTheCookie=()=>{
        setCookie(this.cookieName,'yes',7);
    }

    validateRobot=()=>{
        const that = this;
        const parentform =document.querySelector(that.robotForm);
        const notRobotSubmit=parent.querySelector(that.notRobotSubmit);
        parentform.addEventListener('submit',()=>{
        })
    }


    clickEvent = ()=>{
        const that=this;
        const parent =document.querySelector(that.modalDiv);
        const formSubmit=parent.querySelector(that.formNewsletter);

        parent.querySelector('.close-btn').addEventListener("click",()=>{
            this.setTheCookie();
            parent.style.display = 'none';
        })
        if(!getCookie(that.cookieName)){
            formSubmit.addEventListener("submit",(e)=>{  
                var inputText=document.querySelector(that.newsletterEmail);
                const parent =document.querySelector(that.modalDiv);
                var errorMsgNewsletter=document.querySelector(that.newsletterErrorMsg);
                var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if(!(inputText.value.match(mailformat)))
                { 
                    e.preventDefault();
                    errorMsgNewsletter.style.display='block';
                    errorMsgNewsletter.innerText="Enter Valid Email";
                }
            })
        }
    }
}

setTimeout(() => {
    if(!window.location.href.includes('challenge')){
        new Newsletterpopup;
    }
}, time);
