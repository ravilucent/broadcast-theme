class Slideshow {
    constructor() {
      this.init();
    }

    init = () => {
      let transition= $(".main-crousel").attr('data-transition');
      let arrow= $(".main-crousel").attr('data-arrows');
      let seconds = $(".main-crousel").attr('data-time');
      let autoply = $(".main-crousel").attr('data-autoplay');

      seconds=seconds*1000;

      let bool;
      
       if(transition === "fade"){
           bool=true;
       }else{
           bool=false;
       }

      $(".main-crousel").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows:arrow,
        infinite: true,
        fade: bool,
        autoplay:autoply,
        autoplaySpeed: seconds,
        cssEase: 'linear'
      });

    };
  }

new Slideshow;