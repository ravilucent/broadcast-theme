class Review {
    constructor() {
      this.init();
    }

    init = () => {
      $(".review-slider-slick").slick({
        arrows:false,
        slidesToShow: 1,
        slidesToScroll: 1,
        vertical: true,
        infinite: true,
        speed: 500,
        autoplay:true
      });

    };
  }
  
  new Review();