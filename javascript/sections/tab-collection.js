class Tab {
    constructor() {
      this.init();
    }

    init = () => {
        $(".grid-container").slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows:false,
            infinite:false
          });
    }
}
new Tab();

const tabs = document.querySelector(".tabs-collection");
const tabsBtns = tabs.querySelectorAll(".tab-link");
const tabsContents = tabs.querySelectorAll(".tab-content");


function displayCurrentTab(current) {
    for (let i = 0; i < tabsContents.length; i++) {
        tabsContents[i].style.visibility = (current === i) ? "visible" : "hidden";
        if(tabsBtns[i].classList.contains('current')){
            tabsBtns[i].classList.remove('current');
        }
    }
    tabsBtns[current].classList.add('current');
}
displayCurrentTab(0);

tabs.addEventListener("click", event => {
  if (event.target.className === "tab-link") {
    for (let i = 0; i < tabsBtns.length; i++) {
      if (event.target === tabsBtns[i]) {
        displayCurrentTab(i);
        break;
      }
    }
  }
});
