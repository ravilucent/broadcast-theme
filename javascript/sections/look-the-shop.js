class Shopthelook{
    constructor() {
      this.init();
    }

    init = () => {
        $(".look-slider-slick").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows:true,
            dots: true,
            infinite: true
          });
    }
}
new Shopthelook;