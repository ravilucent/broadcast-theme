window.$ = window.jQuery = $;
/*---------common------------------- */
require('./helper');


/*---------vendors------------------- */


require('./vendors/lazy-load');
require('./vendors/slick');

/*---------snnipets------------------- */

require('./snipets/newsletter-popups');

/*---------sections------------------- */
require('./sections/header');
require('./sections/slid-show');
require('./sections/tab-collection');
require('./sections/look-the-shop');
require('./sections/review');
require ("../scss/app.scss");
